import React from "react";
import { Button, Typography, Container, Box } from "@mui/material";
import UserProfile from "./UserProfile";

export default function Bmi() {
  var weight = UserProfile.getWeight();
  var height = UserProfile.getHeight();
  var bmi = (weight / Math.pow(height / 100, 2)).toFixed(1);
  var mensaje = "";
  var mensaje2 = "";

  if (bmi < 16) {
    mensaje = "Delgadez severa";
    mensaje2 =
      "Ahora mismo tienes una delgadez servera, deberías consultar a un especialista para mejorar tus hábitos alimenticios. Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo";
  } else if (bmi < 18.5) {
    mensaje = "Delgadez moderada";
    mensaje2 =
      "Tienes una delgadez moderada, por lo cual es recomendable que comieras más variado y hacer un superhabit calorico para mejorar tu salud. Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo.";
  } else if (bmi < 25) {
    mensaje = "Peso normal";
    mensaje2 =
      "Tienes un peso normal, podrías aumentar tu masa un poco más si haces un superhabit calórico. Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo.";
  } else if (bmi < 30) {
    mensaje = "Sobrepeso";
    mensaje2 =
      "Estas con sobre peso, deberías de hacer un ejercio y mejorar tus habitos alimenticios para mejorar tu salud. Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo.";
  } else if (bmi < 35) {
    mensaje = "Obesidad grado I";
    mensaje2 =
      "Tienes sobre peso de grado I, lo que significa que el ejercicio físico y mejorar tus habitos alimenticios te serías de utilidad para mejorar tu salud. Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo.";
  } else if (bmi < 40) {
    mensaje = "Obesidad grado II";
    mensaje2 =
      "Tienes obesiddad de grado II, lo que significa que tu salud está afectada por el sobre peso, deberías ejercitar tu cuerpo y mejorar tu alimentación para mejorar tu condición física y mental.Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo.";
  } else {
    mensaje = "Obesidad grado III";
    mensaje2 =
      "Tienes obesidad de grado III, lo que siginca que tu salud peligra por tu sobre peso, deberías consultar a un especialista para mejorar tu condición física y que no peligre tu salud. Aquellas personas que mantienen un peso óptimo y saludable no precisan de intervención terapéutica, pero es importante recomendar un mantenimiento de hábitos saludables de dieta y ejercicio a  aquellas que se posicionan en la franja más alta del intervalo.";
  }

  return (
    <>
      <div>
        <Typography
          sx={{
            fontSize: "2rem",
            fontWeight: 700,
          }}
          gutterBottom
        >
          Hola {UserProfile.getName()} {UserProfile.getLastName()} segun tu
          altura {UserProfile.getHeight()} cm y tu peso{" "}
          {UserProfile.getWeight()} kg tu imc es de:
        </Typography>
        <Typography
          sx={{
            textAlign: "center",
            fontSize: "3rem",
            fontWeight: 700,
            letterSpacing: "-0.3rem",
            display: "inline-block",
            whiteSpace: "nowrap",
            align: "center",
          }}
          gutterBottom
        >
          I M C ==&gt; {bmi}
        </Typography>
        <Box>
          <Box component="img" src="/static/tablaimc.png" alt="logo" />
        </Box>
        <Typography
          sx={{
            textAlign: "center",
            fontSize: "3rem",
            fontWeight: 700,
            letterSpacing: "0.2rem",
            display: "inline-block",
            whiteSpace: "nowrap",
            align: "center",
          }}
          gutterBottom
        >
          {mensaje}
        </Typography>
        <p>{mensaje2}</p>
      </div>
    </>
  );
}
